let mix = require('laravel-mix');
var elixir = require('laravel-elixir');
require('laravel-elixir-imagemin');
/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */
elixir.config.images = {
    folder: 'img',
    outputFolder: 'img'
};
mix.js('resources/assets/js/app.js', 'public/js').version();
mix.sass('resources/assets/sass/app.scss', 'public/css').version();
elixir(function(mix) {
   mix.imagemin();
});
mix.copyDirectory('resources/assets/img', 'public/img');

