<?php
// Route::group(['middleware' => 'guest'], function () {
Route::get('/', 'HomeController@index')->name('home');
Route::get('/items', 'ItemsController@index');
Route::get('/items/{item}', 'ItemsController@show');
// });

// Route::group(['middleware' => 'auth'], function () {
// Route::post('/cart', 'ItemsController@store');
Route::get('/cart', 'CartItemsController@purchase');
Route::post('/cart/checkout', 'CartItemsController@checkout');
Route::post('/add/item', 'ItemsController@addCart');
Route::get('/checkout', 'PurchasesController@index');
Route::get('/partner', function () {
    return view('partner.partner');
});
// });

//Admin
Route::group(['prefix' => 'admin'], function () {
    Voyager::routes();
});
Auth::routes();
