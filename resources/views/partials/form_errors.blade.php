@if(session('errors'))
<div class="alert alert-danger" role="alert">
    <h4 class="alert-heading">
        Oops!
    </h4>
    @foreach(session('errors')->all() as $errors)
    <p class="mb-0">
        <b>
        </b>
        {{ $errors }}
    </p>
    @endforeach
</div>
<br/>
@endif
