<li class="media my-4">
    <img alt="item" class="d-flex mr-3" src="http://via.placeholder.com/180x90"/>
    <div class="media-body d-flex justify-content-around">
        <h3 class="mt-0 mb-1">
            {{ $product->name }}
        </h3>
        <p class="lead">
           RM {{ $product->price }}
        </p>
        <select name="quantity">
            <option value="1">
                1
            </option>
            <option value="2">
                2
            </option>
            <option value="3">
                3
            </option>
            <option value="4">
                4
            </option>
            <option value="5">
                5
            </option>
        </select>
    </div>
    <button aria-label="Close" class="close" id="close" type="button">
        <span aria-hidden="true">
            ×
        </span>
    </button>
</li>
