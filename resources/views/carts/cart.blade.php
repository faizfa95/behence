@extends('layouts.master')

@section('content')
<section class="checkout">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                {{--                @if(count($items)) --}}
                <ul class="list-unstyled">
                    <li class="media my-4">
                        <div class="media-body d-flex justify-content-around ml-auto">
                            <h3 class="text-center">
                                Name
                            </h3>
                            <h3 class="text-center">
                                Price
                            </h3>
                            <h3 class="text-center">
                                Quantity
                            </h3>
                        </div>
                    </li>
                    @foreach($items as $item)
                    @foreach(App\Item::where('id', $item->item_id)->get() as  $product)
                    <form action="cart/checkout" method="POST">
                        @include('carts.listcart')
                    </form>
                    @endforeach
                    @endforeach
                </ul>
                {{-- @endif --}}
                <div class="d-flex justify-content-end">
                    <form class="list-item">
                        {{ csrf_field() }}
                        <input class="item-id" type="hidden" value="{{ $item->id }}"/>
                        <input class="user-id" type="hidden" value="{{ Auth::user()->id  }}"/>
                        <button class="btn btn-default" type="submit">
                            Checkout
                        </button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection
