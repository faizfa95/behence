<header>
    <nav class="navbar navbar-toggleable-md">
        <div class="container">
            <a class="navbar-brand" href="/">
                <img alt="logo" class="d-inline-block align-top" src="img/behance.png"/>
            </a>
            <div class="collapse navbar-collapse" >
                <ul class="navbar-nav my-auto mr-auto">
                    <li class="nav-item dropdown">
                        <a aria-expanded="false" aria-haspopup="true" class="nav-link dropdown-toggle" data-toggle="dropdown" href="http://example.com" id="navbarDropdownMenuLink">
                            Item
                        </a>
                        <div aria-labelledby="navbarDropdownMenuLink" class="dropdown-menu">
                            <a class="dropdown-item" href="#">
                                Action
                            </a>
                            <a class="dropdown-item" href="#">
                                Another action
                            </a>
                            <a class="dropdown-item" href="#">
                                Something else here
                            </a>
                        </div>
                    </li>
                </ul>
                <form class="w-100 my-auto d-inline">
                    <input class="form-control " placeholder=" Search" type="text"/>
                </form>
                <ul class="navbar-nav justify-content-end my-auto">
                    @if (Auth::check())
                    <li class="nav-item">
                        <div class="nav-link" href="#" title="wishlist">
                           {{ Auth::user()->name }}
                        </div>
                    </li>
                    @endif
                    <li class="nav-item dropdown">
                        <a aria-expanded="false" aria-haspopup="true" class="nav-link" data-toggle="dropdown" href="http://example.com" id="navbarDropdownMenuLink">
                            <i aria-hidden="true" class="fa fa-user-o">
                            </i>
                        </a>
                        <div aria-labelledby="navbarDropdownMenuLink" class="dropdown-menu">
                        @if (Auth::check())
                            <a class="dropdown-item" href="{{ route('logout') }}"
                                onclick="event.preventDefault();
                                document.getElementById('logout-form').submit();">
                                Logout
                                 <form id="logout-form" action="{{ route('logout') }}" method="POST" style=      display: none;">
                                        {{ csrf_field() }}
                                 </form>
                            </a>
                            @endif
                            <a class="dropdown-item" href="#">
                                Something else here
                            </a>
                        </div>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="#" title="wishlist">
                            <i aria-hidden="true" class="fa fa-heart-o">
                            </i>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="{{ url('/cart') }}" title="cart">
                            <i aria-hidden="true" class="fa fa-shopping-cart">
                            </i>
                        </a>
                    </li>
                </ul>
            </div>
        </div>
        <div class="mobile">
            <button aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation" class="navbar-toggler navbar-toggler-right" data-target="#navbarSupportedContent" data-toggle="collapse" type="button">
                <i class="fa fa-ellipsis-v" aria-hidden="true"></i>
            </button>
            <a class="navbar-brand" href="/">
                <img alt="logo" class="d-inline-block align-top" src="img/behance.png"/>
            </a>
            <div class="collapse navbar-collapse" id="navbarSupportedContent">
                <ul class="navbar-nav mr-auto">
                   <li class="nav-item">
                        <a class="nav-link" href="#" title="user">
                            User
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="#" title="wishlist">
                           wishlist
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="#" title="cart">
                            cart
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="#" title="cart">
                           item
                        </a>
                    </li>

                </ul>
                <form class="w-100 my-auto d-inline">
                    <input class="form-control mr-sm-2" placeholder=" Search" type="text"/>
                </form>
            </div>
        </div>
    </nav>
</header>

