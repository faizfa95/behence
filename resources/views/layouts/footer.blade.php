<footer>
    <div class="container">
        <div class="row" id="row1">
            <!--CONTACT US-->
            <div class="col-md-4">
                <div class="text-left pb-2">
                    <h4>
                        Contact
                    </h4>
                </div>
                <ul class="list-unstyled">
                    <li class="p-2">
                        <i aria-hidden="true" class="fa fa-envelope">
                        </i>
                        <a href="#" title="email">
                            <span class="pl-2">
                                info@behence.com
                            </span>
                        </a>
                    </li>
                    <li class="p-2">
                       <i aria-hidden="true" class="fa fa-phone">
                        </i>
                        <a href="#" title="phone">
                            <span class="pl-2">
                                03 345 6789 0112
                            </span>
                        </a>
                    </li>
                    <li class="p-2">
                       <i aria-hidden="true" class="fa fa-map-marker">
                       </i>
                        <a href="#" title="Location">
                            <span class="pl-2">
                                Greenplatz Ben 29, Germany 60435
                            </span>
                        </a>
                    </li>
                </ul>
                <div class="icon d-flex flex-row py-2 pl-0">
                    <a class="p-2" href="#" title="facebook">
                        <img alt="facebook" src="img/facebook.png"/>
                    </a>
                    <a class="p-2" href="#" title="twitter">
                        <img alt="twitter" src="img/twitter.png"/>
                    </a>
                    <a class="p-2" href="#" title="instagram">
                        <img alt="instagram" src="img/instagram.png"/>
                    </a>
                    <a class="p-2" href="#" title="google-plus">
                        <img alt="google-plus" src="img/google-plus.png"/>
                    </a>
                </div>
            </div>
            <!--BUSINESS COORPRATE-->
            <div class="col-md-4">
                <!--TITLE-->
                <div class="text-left pb-2">
                    <h4>
                        Business Cooprate
                    </h4>
                </div>
                <!--LIST-->
                <div class="d-flex flex-row">
                    <ul class="list-unstyled">
                        <li class="p-2">
                            <i aria-hidden="true" class="fa fa-angle-double-right">
                            </i>
                            <a href="#">
                                Web Design
                            </a>
                        </li>
                        <li class="p-2">
                            <i aria-hidden="true" class="fa fa-angle-double-right">
                            </i>
                            <a href="#">
                                About Templete
                            </a>
                        </li>
                        <li class="p-2">
                            <i aria-hidden="true" class="fa fa-angle-double-right">
                            </i>
                            <a href="#">
                                Mobile
                            </a>
                        </li>
                        <li class="p-2">
                            <i aria-hidden="true" class="fa fa-angle-double-right">
                            </i>
                            <a href="#">
                                About Templete
                            </a>
                        </li>
                        <li class="p-2">
                            <i aria-hidden="true" class="fa fa-angle-double-right">
                            </i>
                            <a href="#">
                                About Templete
                            </a>
                        </li>
                    </ul>
                </div>
            </div>
            <!--FAQ-->
            <div class="col-md-4">
                <!--TITLE-->
                <div class="text-left pb-2">
                    <h4>
                        FAQ
                    </h4>
                </div>
                <!--LIST-->
                <div class="d-flex flex-row">
                    <ul class="list-unstyled">
                        <li class="p-2">
                            <i aria-hidden="true" class="fa fa-angle-double-right">
                            </i>
                            <a href="#">
                                Web Design
                            </a>
                        </li>
                        <li class="p-2">
                            <i aria-hidden="true" class="fa fa-angle-double-right">
                            </i>
                            <a href="#">
                                About Templete
                            </a>
                        </li>
                        <li class="p-2">
                            <i aria-hidden="true" class="fa fa-angle-double-right">
                            </i>
                            <a href="#">
                                Mobile
                            </a>
                        </li>
                        <li class="p-2">
                            <i aria-hidden="true" class="fa fa-angle-double-right">
                            </i>
                            <a href="#">
                                About Templete
                            </a>
                        </li>
                        <li class="p-2">
                            <i aria-hidden="true" class="fa fa-angle-double-right">
                            </i>
                            <a href="#">
                                About Templete
                            </a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</footer>
