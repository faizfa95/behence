<!-- Bootstrap core JavaScript
    ================================================== -->
<!-- Placed at the end of the document so the pages load faster -->
<script src="/bower_components/jquery/dist/jquery.min.js">
</script>
<script src="/bower_components/tether/dist/js/tether.min.js">
</script>
<script src="/bower_components/bootstrap/dist/js/bootstrap.min.js">
</script>
<script src="js/app.js" type="text/javascript">
</script>
<script>
    jQuery(document).ready(function($) {

        $('.list-item').submit(function(e) {
            alert("Item was Added");
            e.preventDefault();
            var form = $(this);
            var userId = form.find('.user-id').val();
            var itemId = form.find('.item-id').val();
            var _token = form.find('input[name=_token]').val();
            $.ajax({
                url: 'add/item',
                type: 'post',
                data: {
                    userId: userId,
                    itemId: itemId,
                    _token: _token
                },
            }).done(function(reponse) {
                console.log(reponse);
            });
            // }).fail(function() {

            //     alert("Sorry, item cannot be added");
            // }).always(function() {
            //     console.log("added");
            // });
        });
    });
</script>
