<!DOCTYPE doctype html>
<html lang="{{ app()->getLocale() }}">
    <head>
        <meta charset="utf-8">
            <meta content="IE=edge" http-equiv="X-UA-Compatible">
                <meta content="width=device-width, initial-scale=1" name="viewport">
                    <title>
                        Laravel
                    </title>
                    <link href="css/app.css" rel="stylesheet" type="text/css"/>
                </meta>
            </meta>
        </meta>
    </head>
    <body>
        <div class="flex-center position-ref full-height">
            @if (Route::has('login'))
            <div class="top-right links">
                @auth
                <a href="{{ url('/home') }}">
                    Home
                </a>
                @else
                <a href="{{ route('login') }}">
                    Login
                </a>
                <a href="{{ route('register') }}">
                    Register
                </a>
                @endauth
            </div>
            @endif
            <div class="content">
                <div class="title m-b-md">
                    Laravel
                </div>
                <div class="links">
                    <a href="https://laravel.com/docs">
                        Documentation
                    </a>
                    <a href="https://laracasts.com">
                        Laracasts
                    </a>
                    <a href="https://laravel-news.com">
                        News
                    </a>
                    <a href="https://forge.laravel.com">
                        Forge
                    </a>
                    <a href="https://github.com/laravel/laravel">
                        GitHub
                    </a>
                </div>
            </div>
        </div>
        <script src="js/app.js" type="text/javascript">
        </script>
    </body>
</html>
