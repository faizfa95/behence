@extends('layouts.master')

@section('content')
<section class="list">
   <div class="container">
      <!--CARD ITEM-->
        <div class="row">
            @foreach($items as $item)
               @include('items.carditem')
            @endforeach
        </div>
        {{-- <div class="row">
            @foreach($items as $item)
               @include('items.carditem')
            @endforeach
        </div>
        <div class="row">
            @foreach($items as $item)
               @include('items.carditem')
            @endforeach
        </div> --}}
   </div>
</section>
@endsection
