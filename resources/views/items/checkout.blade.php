@extends('layouts.master')

@section('content')
<section class="checkout">
    <div class="container">
        <div class="row">
            <div class="col-md-8">
                <ul class="list-unstyled">
                    <li class="media">
                        <img alt="item" class="d-flex mr-3" src="http://via.placeholder.com/180x90"/>
                        <div class="media-body">
                            <h3 class="mt-0 mb-1">
                                List-based media object
                            </h3>
                            <p class="lead my-0">
                                quantity
                            </p>
                            <p class="lead">
                                price
                            </p>
                        </div>
                    </li>
                    <li class="media my-4">
                        <img alt="item" class="d-flex mr-3" src="http://via.placeholder.com/180x90"/>
                        <div class="media-body">
                            <h3 class="mt-0 mb-1">
                                List-based media object
                            </h3>
                            <p class="lead my-0">
                                quantity
                            </p>
                            <p class="lead">
                                price
                            </p>
                        </div>
                    </li>
                    <li class="media">
                        <img alt="item" class="d-flex mr-3" src="http://via.placeholder.com/180x90"/>
                        <div class="media-body">
                            <h3 class="mt-0 mb-1">
                                List-based media object
                            </h3>
                            <p class="lead my-0">
                                quantity
                            </p>
                            <p class="lead">
                                price
                            </p>
                        </div>
                    </li>
                </ul>
            </div>
            <div class="col-md-4">
                <h4 class="pb-4">
                    BILLING INFORMATION
                </h4>
                <p>
                    Aute qui laborum veniam irure ut in exercitation sed aliquip tempor cillum duis aliqua.
                </p>
                <p>
                    Officia fugiat reprehenderit excepteur pariatur magna consequat excepteur exercitation sit excepteur enim ullamco adipisicing velit sed do ad enim nostrud nostrud non.
                </p>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <h3>
                    PAYMENT METHOD
                </h3>
                <form>
                    <div class="form-check form-check-inline">
                        <label class="form-check-label">
                            <input class="form-check-input" id="creditcard" name="creditcard" type="radio" value="creditcard"/>
                            Credit Card
                        </label>
                    </div>
                    <div class="form-check form-check-inline">
                        <label class="form-check-label">
                            <input class="form-check-input" id="bank" name="bank" type="radio" value="bank"/>
                            Bank
                        </label>
                    </div>
                </form>
            </div>
        </div>
    </div>
</section>
@endsection
