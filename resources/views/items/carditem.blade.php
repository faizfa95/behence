<div class="col-md-3">
    <div class="card">
        <a href="/items/{{ $item->name }}">
            <img alt="Card item" class="img-thumbnail" src="http://via.placeholder.com/350x350"/>
        </a>
        <h3 class="card-title pl-3">
            {{ $item->name }}
        </h3>
        <div class="d-flex">
            <div class="pl-3 w-100">
                <form class="list-item">
                    {{ csrf_field() }}
                    <input class="item-id" type="hidden" value="{{ $item->id }}"/>
                    <input class="user-id" type="hidden" value="{{ Auth::user()->id  }}"/>
                    <button class="btn cart w-100" id="item-add" type="submit">
                        Add to cart
                    </button>
                </form>
            </div>
            <div class="pl-1 pr-3 justify-content-end">
                <button class="btn btn-danger wishlist" type="button">
                    <i aria-hidden="true" class="fa fa-heart-o">
                    </i>
                </button>
            </div>
        </div>
    </div>
</div>
