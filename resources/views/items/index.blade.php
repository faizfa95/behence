@extends('layouts.master')

@section('content')
<!--BANNER SEC-->
<section class="jumbotron text-center">
    <div class="container">
        <h1 class="jumbotron-heading">
            Album example
        </h1>
        <p class="lead text-muted">
            Something short and leading about the collection below—its contents, the creator, etc. Make it short and sweet, but not too short so folks don't simply skip over it entirely.
        </p>
    </div>
</section>
<!--ABOUT SEC-->
<section class="about">
    <div class="container">
        <div class="row">
            <div class="col-md-6">
                <h2>
                    About
                </h2>
                <p>
                    Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec ac nunc suscipit odio sodales efficitur. Praesent maximus laoreet lectus congue tincidunt. Mauris ac dapibus mauris, in scelerisque libero. Praesent et sodales massa. Quisque imperdiet lorem a posuere pretium. Quisque euismod sit amet mi nec pulvinar. Praesent elementum sem sit amet mauris congue fringilla. Quisque placerat id urna non suscipit. Phasellus nunc neque, laoreet vitae posuere ut, laoreet eu odio. Nunc scelerisque accumsan rhoncus. Vestibulum tincidunt sit amet risus at pellentesque.
                </p>
            </div>
            <div class="col-md-6">
                <img alt="about" class="img-fluid w-100 h-100" src="http://via.placeholder.com/650x650"/>
            </div>
        </div>
    </div>
</section>
<!--BEST PRODUCT SEC-->
<section class="hottest">
    <div class="container">
        <!--TITLE-->
        <div class="row">
            <div class="col-md-12">
                <h2 class="text-center my-5">
                    HOTTEST
                </h2>
            </div>
        </div>
        <!--CARD ITEM-->
        <div class="row">
            <div class="col-md-4">
                <div class="card">
                    <img alt="Card item" class="img-thumbnail" src="http://via.placeholder.com/350x350"/>
                    <h3 class="card-title pl-3">
                        Price
                    </h3>
                    <div class="d-flex">
                        <div class="pl-3 w-100">
                            <button class="btn btn-danger cart w-100" type="button">
                                Add to cart
                            </button>
                        </div>
                        <div class="pl-1 pr-3 justify-content-end">
                            <button class="btn btn-danger wishlist" type="button">
                                <i aria-hidden="true" class="fa fa-heart-o">
                                </i>
                            </button>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <div class="card">
                    <img alt="Card item" class="img-thumbnail" src="http://via.placeholder.com/350x350"/>
                    <h3 class="card-title pl-3">
                        Price
                    </h3>
                    <div class="d-flex">
                        <div class="pl-3 w-100">
                            <button class="btn btn-danger cart w-100" type="button">
                                Add to cart
                            </button>
                        </div>
                        <div class="pl-1 pr-3 justify-content-end">
                            <button class="btn btn-danger wishlist" type="button">
                                <i aria-hidden="true" class="fa fa-heart-o">
                                </i>
                            </button>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <div class="card">
                    <img alt="Card item" class="img-thumbnail" src="http://via.placeholder.com/350x350"/>
                    <h3 class="card-title pl-3">
                        Price
                    </h3>
                    <div class="d-flex">
                        <div class="pl-3 w-100">
                            <button class="btn btn-danger cart w-100" type="button">
                                Add to cart
                            </button>
                        </div>
                        <div class="pl-1 pr-3 justify-content-end">
                            <button class="btn btn-danger wishlist" type="button">
                                <i aria-hidden="true" class="fa fa-heart-o">
                                </i>
                            </button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!--NEW PRODUCT SEC-->
<section class="hottest">
    <div class="container">
        <!--TITLE-->
        <div class="row">
            <div class="col-md-12">
                <h2 class="text-center my-5">
                    NEWEST
                </h2>
            </div>
        </div>
        <!--CARD ITEM-->
        <div class="row">
            <div class="col-md-4">
                <div class="card">
                    <img alt="Card item" class="img-thumbnail" src="http://via.placeholder.com/350x350"/>
                    <h3 class="card-title pl-3">
                        Price
                    </h3>
                    <div class="d-flex">
                        <div class="pl-3 w-100">
                            <button class="btn btn-danger cart w-100" type="button">
                                Add to cart
                            </button>
                        </div>
                        <div class="pl-1 pr-3 justify-content-end">
                            <button class="btn btn-danger wishlist" type="button">
                                <i aria-hidden="true" class="fa fa-heart-o">
                                </i>
                            </button>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <div class="card">
                    <img alt="Card item" class="img-thumbnail" src="http://via.placeholder.com/350x350"/>
                    <h3 class="card-title pl-3">
                        Price
                    </h3>
                    <div class="d-flex">
                        <div class="pl-3 w-100">
                            <button class="btn btn-danger cart w-100" type="button">
                                Add to cart
                            </button>
                        </div>
                        <div class="pl-1 pr-3 justify-content-end">
                            <button class="btn btn-danger wishlist" type="button">
                                <i aria-hidden="true" class="fa fa-heart-o">
                                </i>
                            </button>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <div class="card">
                    <img alt="Card item" class="img-thumbnail" src="http://via.placeholder.com/350x350"/>
                    <h3 class="card-title pl-3">
                        Price
                    </h3>
                    <div class="d-flex">
                        <div class="pl-3 w-100">
                            <button class="btn btn-danger cart w-100" type="button">
                                Add to cart
                            </button>
                        </div>
                        <div class="pl-1 pr-3 justify-content-end">
                            <button class="btn btn-danger wishlist" type="button">
                                <i aria-hidden="true" class="fa fa-heart-o">
                                </i>
                            </button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!--PROMOTION-->
<section class="promotion">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="d-flex justify-content-end">
                    <div class="mr-auto my-auto">
                        <h2 class="text-left">
                            PROMOTION
                        </h2>
                    </div>
                    <div class="p-2">
                        <img alt="item promo" class="img-thumbnail border-0 w-100 h-100" src="http://via.placeholder.com/94x94"/>
                    </div>
                    <div class="p-2">
                        <img alt="item promo" class="img-thumbnail border-0 w-100 h-100" src="http://via.placeholder.com/94x94"/>
                    </div>
                    <div class="p-2">
                        <img alt="item promo" class="img-thumbnail border-0 w-100 h-100" src="http://via.placeholder.com/94x94"/>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection
