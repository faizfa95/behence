@extends('layouts.master')

@section('content')
<section class="item">
    <div class="container">
        <div class="row">
            <div class="col-md-6">
                <img alt="item" class="img-fluid w-100 h-100" src="http://via.placeholder.com/650x650"/>
            </div>
            <div class="col-md-6">
                <h2 class="display-4">
                   {{ $item->name }}
                </h2>
                <h3>
                   MYR {{ $item->price }}
                </h3>
                <p>
                    {{ $item->caption }}
                </p>
                <h3>
                    Category:
                    <span>
                        {{ $item->category }}
                    </span>
                </h3>
                <form class="list-item">
                    {{ csrf_field() }}
                    <input class="item-id" type="hidden" value="{{ $item->id }}"/>
                    <input class="user-id" type="hidden" value="{{ Auth::user()->id  }}"/>
                    <button class="btn cart" id="item-add" type="submit">
                        Add to cart
                    </button>
                </form>
            </div>
        </div>
    </div>
</section>
<section class="related">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h2 class="text-center my-5">
                    RELATED ITEM
                </h2>
            </div>
        </div>
        <div class="row">
            @include('items.carditem')
        </div>
    </div>
</section>
@endsection
