@extends('layouts.master')

@section('content')
<section class="login">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                @include('partials.form_errors')
            </div>
        </div>
        <div class="row">
            <!--LOGIN-->
            <div class="col-md-6">
                <div class="py-3">
                    <h2 class="display-3">
                        Login
                    </h2>
                </div>
                <form action="{{ route('login') }}" class="form-horizontal py-3" method="POST">
                    {{ csrf_field() }}
                    <div class="form-group">
                        <label for="email">
                            Email
                        </label>
                        <input class="form-control" id="email" name="email" type="email"/>
                    </div>
                    <div class="form-group">
                        <label for="password">
                            Password
                        </label>
                        <input class="form-control" id="password" name="password" type="password"/>
                    </div>
                    <div class="form-group">
                        <button class="btn btn-danger" type="submit">
                            Login
                        </button>
                    </div>
                </form>
            </div>
            <!--SIGN UP-->
            <div class="col-md-6">
                <div class="py-3">
                    <h2 class="display-3">
                        Sign Up
                    </h2>
                </div>
                <form action="{{ route('register') }}" class="form-horizontal py-3" method="POST">
                {{ csrf_field() }}
                <div class="form-group">
                        <label for="name">
                            Name
                        </label>
                        <input class="form-control" id="name" name="name" type="text"/>
                </div>
                    <div class="form-group">
                        <label for="email">
                            Email
                        </label>
                        <input class="form-control" id="email" name="email" type="email"/>
                    </div>
                    <div class="form-group">
                        <label for="password">
                            Password
                        </label>
                        <input class="form-control" id="password" name="password" type="password"/>
                    </div>
                    <div class="form-group">
                        <label for="password_confirmation">
                            Password Confirmation
                        </label>
                        <input class="form-control" id="password_confirmation" name="password_confirmation" type="password"/>
                    </div>
                    <div class="form-group">
                        <a href="{{ route('password.request') }}">
                            <button class="btn btn-danger" type="submit">
                                Signup
                            </button>
                        </a>
                    </div>
                </form>
            </div>
        </div>
    </div>
</section>
@endsection
