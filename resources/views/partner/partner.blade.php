@extends('layouts.master')

@section('content')
<section class="partner">
    <div class="container">
     <div class="row">
            <div class="col-md-12">
                <h2 class="text-center">
                     PARTNERSHIP LIST
                </h2>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6">
                <img alt="about" class="img-fluid w-100 h-100" src="http://via.placeholder.com/650x650"/>
            </div>
            <div class="col-md-6">
                <h2>
                    About
                </h2>
                <p>
                    Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec ac nunc suscipit odio sodales efficitur. Praesent maximus laoreet lectus congue tincidunt. Mauris ac dapibus mauris, in scelerisque libero. Praesent et sodales massa. Quisque imperdiet lorem a posuere pretium. Quisque euismod sit amet mi nec pulvinar. Praesent elementum sem sit amet mauris congue fringilla. Quisque placerat id urna non suscipit. Phasellus nunc neque, laoreet vitae posuere ut, laoreet eu odio. Nunc scelerisque accumsan rhoncus. Vestibulum tincidunt sit amet risus at pellentesque.
                </p>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6">
                <img alt="about" class="img-fluid w-100 h-100" src="http://via.placeholder.com/650x650"/>
            </div>
            <div class="col-md-6">
                <h2>
                    About
                </h2>
                <p>
                    Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec ac nunc suscipit odio sodales efficitur. Praesent maximus laoreet lectus congue tincidunt. Mauris ac dapibus mauris, in scelerisque libero. Praesent et sodales massa. Quisque imperdiet lorem a posuere pretium. Quisque euismod sit amet mi nec pulvinar. Praesent elementum sem sit amet mauris congue fringilla. Quisque placerat id urna non suscipit. Phasellus nunc neque, laoreet vitae posuere ut, laoreet eu odio. Nunc scelerisque accumsan rhoncus. Vestibulum tincidunt sit amet risus at pellentesque.
                </p>
            </div>
        </div>
    </div>
</section>
@endsection
