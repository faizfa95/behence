<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CartItems extends Model
{
    protected $table = 'cart_items';
    public $timestamps = false;

    public function item()
    {
       return $this->hasMany(Item::class);
    }


}
