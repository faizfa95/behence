<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Order;

class OrderItem extends Model
{
    protected $table = 'order_items';
    public $timestamps = false;

    public function item()
    {
        return $this->hasOne(Order::class);
    }
}
