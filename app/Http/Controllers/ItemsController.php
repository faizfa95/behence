<?php

namespace App\Http\Controllers;

use App\Cart as Cart;
use App\CartItems as CartItems;
use App\Item as Item;
use Illuminate\Http\Request;

class ItemsController extends Controller
{
    public function index()
    {
        $items = Item::get();
        return view('items.list', compact('items'));
    }
    public function show(Item $item)
    {
        return view('items.item', compact('item'));
    }

    public function store()
    {
        request()->all();
        return view('items.cart');
    }
    public function addCart(Request $request)
    {
        $itemId = $request->itemId;
        $userId = $request->userId;

        if (empty(Cart::where('user_id', $userId)->count())) {
            // mkae new cart
            $cart          = new Cart;
            $cart->user_id = $userId;

            $cart->save();

            $cart->where('user_id', $userId)->get();
            $cartId = $cart->cart_id;

            $cartItems          = new CartItems;
            $cartItems->item_id = $itemId;
            $cartItems->cart_id = $cartId;

            $cartItems->save();

        } else {

            $cart     = new Cart;
            $cart_ids = $cart->where('user_id', $userId)->first();
            $cartId   = $cart_ids->cart_id;

            // Check if item exists in the cart
            $cartItems = new CartItems;
            $carts     = $cartItems->where('cart_id', $cartId)->where('item_id', $itemId)->get();

            if (!$carts->count()) {
                // add items to that cart
                $cartItems->item_id = $itemId;
                $cartItems->cart_id = $cartId;

                $cartItems->save();
                echo "Added for the first time";
            } else {
                echo "Already added!";
            }
        }
    }
}
