<?php

namespace App\Http\Controllers;

use App\Cart;
use App\CartItems;
use Auth;

class CartItemsController extends Controller
{
    public function purchase()
    {
        $cart   = Cart::where('user_id', Auth::user()->id)->first();
        $cartId = $cart->cart_id;

        $items = CartItems::where('cart_id', $cartId)->get();

        return view('carts.cart', compact('items'));
    }

    public function checkout(Request $request)
    {


    }

}
